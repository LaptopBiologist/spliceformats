# spliceformats
> A set of tools for parser and working with the results of differential splicing analyses, e.g. for MATS output. 


## Install

`spliceformats` relies on both Python and R dependencies. Namely, rpy2 must must installed and the `ashr` package must be installed on the version of R that rpy2 accesses. Rather than manually install all of the dependencies, the simplest way to install `spliceformats` is to first install the a conda enviroment that meets the package requirements, and then install `spliceformats` in that environment using pip. 

First, use the `spliceformats.yaml` file provided in the repository to create a new conda environment:
```
$ conda env create -f spliceformats.yaml -n spliceformats
```

If instead you wish to install the dependencies in an existing environment (named, for example, `my_env`), instead run

```
$ conda env update -n my_env -f spliceformats.yaml 
```

Once the environment is installed/updated, activate the environment (replace `env_name` with the enviroment's name):

```
$ conda activate env_name
```

Then we can install install `spliceformats`. Move into the downloaded `spliceformats` directory, and while in that directory run:

```
$ pip install -e .
```

The period `.` indicates the pip should treat the current directory as the Python package to install. The argument `-e` specifies that the installation should be editable--if the contents of the ./spliceformats directory changes, so does the installed the library.  

The provided conda environment includes jupyter and jupyter lab, so jupyter notebooks that can access the library can be opened using

```
$ jupyter lab
```

## Experimental modules

There are additional modules for working directly with `alignments` and representing `annotations` as `TranscriptomeGraphs`, but these are still experimental and should be used with caution.

## How to use

```python
from matplotlib import pyplot
import numpy
import scipy
import seaborn


```

### Working with rMATS outputs

The `read_rmats` function can be used to load the output of an rMATS analysis. It can be used to load a single rMATS file:

```python
from spliceformats.readMATS import read_rmats
data=read_rmats('00_data/SE.MATS.JunctionCountOnly.txt')

```

Or all of the rMATS tables stored in a tar archive:

```python
data=read_rmats('00_data/ENCFF602CHO.tar.gz')
```

Each of the different classes of splicing events (SE, MXE, A5SS, A3SS, RI) as stored in `rmatsEvent` classes, which are grouped together in an `rmatsResult` object.

```python
print(type(data))
print (type(data.SE))
```

    <class 'spliceformats.readMATS.rmatsResults'>
    <class 'spliceformats.readMATS.rmatsEvents'>


All of the information about a particular class of splicing event can be accessed in the 

.SE

.A3SS

.A5SS

.MXE

.RI

attributes of the rmatsResults class.

For the example the entire rMATS table is stored as a dataframe, `.df`:


```python
data.SE.df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ID</th>
      <th>GeneID</th>
      <th>geneSymbol</th>
      <th>chr</th>
      <th>strand</th>
      <th>exonStart_0base</th>
      <th>exonEnd</th>
      <th>upstreamES</th>
      <th>upstreamEE</th>
      <th>downstreamES</th>
      <th>...</th>
      <th>SJC_SAMPLE_1</th>
      <th>IJC_SAMPLE_2</th>
      <th>SJC_SAMPLE_2</th>
      <th>IncFormLen</th>
      <th>SkipFormLen</th>
      <th>PValue</th>
      <th>FDR</th>
      <th>IncLevel1</th>
      <th>IncLevel2</th>
      <th>IncLevelDifference</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>12823</td>
      <td>ENSG00000131504.11</td>
      <td>DIAPH1</td>
      <td>chr5</td>
      <td>-</td>
      <td>140967790</td>
      <td>140967817</td>
      <td>140966608</td>
      <td>140966764</td>
      <td>140998364</td>
      <td>...</td>
      <td>74,29</td>
      <td>40,24</td>
      <td>3,6</td>
      <td>126</td>
      <td>100</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>0.07,0.0</td>
      <td>0.914,0.76</td>
      <td>-0.802</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1579</td>
      <td>ENSG00000122591.7</td>
      <td>FAM126A</td>
      <td>chr7</td>
      <td>-</td>
      <td>22986570</td>
      <td>22986866</td>
      <td>22980887</td>
      <td>22985782</td>
      <td>22999874</td>
      <td>...</td>
      <td>26,57</td>
      <td>51,59</td>
      <td>16,18</td>
      <td>199</td>
      <td>100</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>0.019,0.058</td>
      <td>0.616,0.622</td>
      <td>-0.581</td>
    </tr>
    <tr>
      <th>2</th>
      <td>16290</td>
      <td>ENSG00000085733.11</td>
      <td>CTTN</td>
      <td>chr11</td>
      <td>+</td>
      <td>70267575</td>
      <td>70267642</td>
      <td>70266328</td>
      <td>70266616</td>
      <td>70269045</td>
      <td>...</td>
      <td>152,150</td>
      <td>78,128</td>
      <td>20,43</td>
      <td>166</td>
      <td>100</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>0.278,0.241</td>
      <td>0.701,0.642</td>
      <td>-0.412</td>
    </tr>
    <tr>
      <th>3</th>
      <td>16299</td>
      <td>ENSG00000085733.11</td>
      <td>CTTN</td>
      <td>chr11</td>
      <td>+</td>
      <td>70267575</td>
      <td>70267686</td>
      <td>70266505</td>
      <td>70266616</td>
      <td>70269045</td>
      <td>...</td>
      <td>152,150</td>
      <td>342,422</td>
      <td>20,43</td>
      <td>199</td>
      <td>100</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>0.604,0.524</td>
      <td>0.896,0.831</td>
      <td>-0.299</td>
    </tr>
    <tr>
      <th>4</th>
      <td>19274</td>
      <td>ENSG00000111206.8</td>
      <td>FOXM1</td>
      <td>chr12</td>
      <td>-</td>
      <td>2974520</td>
      <td>2974565</td>
      <td>2973848</td>
      <td>2973918</td>
      <td>2975558</td>
      <td>...</td>
      <td>399,520</td>
      <td>159,295</td>
      <td>187,301</td>
      <td>144</td>
      <td>100</td>
      <td>0.000000</td>
      <td>0.0</td>
      <td>0.07,0.082</td>
      <td>0.371,0.405</td>
      <td>-0.312</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>32818</th>
      <td>9990</td>
      <td>ENSG00000083857.9</td>
      <td>FAT1</td>
      <td>chr4</td>
      <td>-</td>
      <td>187518187</td>
      <td>187518325</td>
      <td>187516842</td>
      <td>187516980</td>
      <td>187518835</td>
      <td>...</td>
      <td>3,0</td>
      <td>412,332</td>
      <td>0,0</td>
      <td>199</td>
      <td>100</td>
      <td>1.000000</td>
      <td>1.0</td>
      <td>0.992,1.0</td>
      <td>1.0,1.0</td>
      <td>-0.004</td>
    </tr>
    <tr>
      <th>32819</th>
      <td>9991</td>
      <td>ENSG00000083857.9</td>
      <td>FAT1</td>
      <td>chr4</td>
      <td>-</td>
      <td>187521051</td>
      <td>187521514</td>
      <td>187519125</td>
      <td>187519279</td>
      <td>187522422</td>
      <td>...</td>
      <td>2,0</td>
      <td>208,151</td>
      <td>0,0</td>
      <td>199</td>
      <td>100</td>
      <td>0.847889</td>
      <td>1.0</td>
      <td>0.988,1.0</td>
      <td>1.0,1.0</td>
      <td>-0.006</td>
    </tr>
    <tr>
      <th>32820</th>
      <td>9992</td>
      <td>ENSG00000083857.9</td>
      <td>FAT1</td>
      <td>chr4</td>
      <td>-</td>
      <td>187516294</td>
      <td>187516345</td>
      <td>187510225</td>
      <td>187510374</td>
      <td>187516842</td>
      <td>...</td>
      <td>214,243</td>
      <td>0,0</td>
      <td>154,273</td>
      <td>150</td>
      <td>100</td>
      <td>0.837385</td>
      <td>1.0</td>
      <td>0.018,0.0</td>
      <td>0.0,0.0</td>
      <td>0.009</td>
    </tr>
    <tr>
      <th>32821</th>
      <td>9998</td>
      <td>ENSG00000149548.10</td>
      <td>CCDC15</td>
      <td>chr11</td>
      <td>+</td>
      <td>124862475</td>
      <td>124862583</td>
      <td>124861356</td>
      <td>124861479</td>
      <td>124863064</td>
      <td>...</td>
      <td>0,0</td>
      <td>13,4</td>
      <td>0,2</td>
      <td>199</td>
      <td>100</td>
      <td>1.000000</td>
      <td>1.0</td>
      <td>1.0,1.0</td>
      <td>1.0,0.501</td>
      <td>0.250</td>
    </tr>
    <tr>
      <th>32822</th>
      <td>9999</td>
      <td>ENSG00000149548.10</td>
      <td>CCDC15</td>
      <td>chr11</td>
      <td>+</td>
      <td>124873773</td>
      <td>124873855</td>
      <td>124863064</td>
      <td>124863139</td>
      <td>124875004</td>
      <td>...</td>
      <td>0,1</td>
      <td>49,65</td>
      <td>2,2</td>
      <td>181</td>
      <td>100</td>
      <td>0.742343</td>
      <td>1.0</td>
      <td>1.0,0.956</td>
      <td>0.931,0.947</td>
      <td>0.039</td>
    </tr>
  </tbody>
</table>
<p>32823 rows × 23 columns</p>
</div>



The PSI values for each exon in sample 1 and sample 2 can be accessed in the `.psi1` and `.psi2` attributes.

```python
pyplot.scatter(data.SE.psi2, data.SE.psi1, s=5)
pyplot.xlabel('Control PSI (psi2)')
pyplot.ylabel('KD PSI (psi)')
```




    Text(0, 0.5, 'KD PSI (psi)')




![png](docs/images/output_13_1.png)


Note that by default a pseudocount correction is applied to the PSI estimates (+1 is added to the IJC and SJC read counts) by calling the `estimate_PSI` functiom. This reflects that observing zero inclusion junctions reads is not evidence that PSI truly equals zero. The unadjusted estimates can still be accessed in the `.orig` attribute, which stores an `rmatsEvents` object with the uncorrected data

```python
pyplot.scatter(data.SE.orig.psi2, data.SE.orig.psi1, s=5)
pyplot.xlabel('Control PSI (psi2)')
pyplot.ylabel('KD PSI (psi1)')
pyplot.title("No pseudocount correction")
```




    Text(0.5, 1.0, 'No pseudocount correction')




![png](docs/images/output_15_1.png)


Even with the pseudocounts, these corrected estimates of PSI include noise from variation in the readcounts. Another way to visualize this problem is by plotting the original rMATS deltaPSI against the the total read counts:



```python
sig=data.SE.fdr<.1
pyplot.scatter(data.SE.orig.totalreads, data.SE.orig.dpsi, s=5, alpha=.2)
pyplot.xscale('log')
pyplot.ylabel('dPSI')
pyplot.xlabel('Average IJC + SJC (log10)')
```




    Text(0.5, 0, 'Average IJC + SJC (log10)')




![png](docs/images/output_17_1.png)


It's worth noting that applying pseudocounts (as `spliceformats` does by default) partially resolves this issue, shrinking dPSI a bit toward zero when the read counts are low:


```python
pyplot.figure(figsize=(12,5))
ax=pyplot.subplot(121,)
pyplot.title('Before pseudocounts')
pyplot.scatter(data.SE.orig.totalreads, data.SE.orig.dpsi, s=5, alpha=.2,)
pyplot.xscale('log')
pyplot.ylabel('dPSI')
pyplot.xlabel('Average IJC + SJC (log10)')
pyplot.subplot(122,sharey=ax)
pyplot.title('After pseudocounts')
pyplot.scatter(data.SE.totalreads, data.SE.dpsi, s=5,alpha=.2)
pyplot.xscale('log')
pyplot.ylabel('dPSI')
pyplot.xlabel('Average IJC + SJC (log10)')
```




    Text(0.5, 0, 'Average IJC + SJC (log10)')




![png](docs/images/output_19_1.png)


We can get a cleaner picture of the relationship between $\Psi_1$ and PSI2 by filtering out all splicing events supported on average by fewer than 50 total sequencing reads. We

```python
print ((data.SE.totalreads>50).mean())
```

    0.5258812418121439


About 52% splicing events are supported by enough reads to survive this threshold, and when we plot only those splicing events, it's clear that most exons don't appear to have altered inclusion levels after knockdown, but a subset appear less included after knockdown:

```python
filter_ind=data.SE.totalreads>50
pyplot.scatter(data.SE.psi2[filter_ind], data.SE.psi1[filter_ind], s=5)
pyplot.xlabel('Control PSI (psi2)')
pyplot.ylabel('KD PSI (psi1)')
```




    Text(0, 0.5, 'KD PSI (psi1)')




![png](docs/images/output_23_1.png)


The FDR-adjusted pvalue associated with each splicing event is stored in `.fdr`, so we can highlight which events were called by rMATS as significant.

```python
sig=data.SE.fdr<.1
filter_ind=data.SE.totalreads>50

pyplot.scatter(data.SE.psi2[filter_ind], data.SE.psi1[filter_ind], s=5)
pyplot.scatter(data.SE.psi2[filter_ind & sig ], data.SE.psi1[filter_ind & sig ],c='r', s=10, label='Sig')
pyplot.xlabel('Control PSI (psi2)')
pyplot.ylabel('KD PSI (psi1)')
pyplot.legend()
```




    <matplotlib.legend.Legend at 0x7f79101bbeb0>




![png](docs/images/output_25_1.png)


It would be nice, however, to be able to perform this visualization with needing to throw away a large fraction of the data and without worrying about what that cutoff should be.

### Performing shrinkage on dPSI

A cleaner way of resolving this--which does not require discarding data--is to remove the read count variation from the estimates of the change in PSI. This correction is sometimes called shrinkage and works by using full distribution of parameter estimates along with the uncertainty associated with each estimate to infer the underlying distribution of the true parameter values. The R package `ashr` accomplishes this by modelling the parameter estimates as following a mixture of many distributions, with some fraction of parameters truly equalling zero and others being nonzero. 

Actually performing shrinkage on rMATS deltaPSI requires a bit of statistical gymnastics, as `ashr` requires the standard error associate with each change in inclusion level estimate... which is not provided by rMATS. For details on how rMATS p-values are used to recover the standard error associated with each deltaPSI estimate, see the documentation for `approximate_coefficients`.

All of the necessary acrobatics, however, can be accomplished by calling the `shrink_rMATS` function. This takes either an `rmatsResults` or `rmatsEvents` object and returns a copy where shrinkage has been applied to deltaPSI for all classes of splicing events:

```python
from spliceformats.shrinkage import shrink_rMATS

corr_data=shrink_rMATS(data)
```

Shrinkage is applied only to the change in exon-inclusion (specifically the change in the log-odds of exon-inclusion). This is used to reconstruct an estimate of PSI1 based on the corrected estimate of deltaPSI:

```python
sig=data.SE.fdr<.1
pyplot.scatter(corr_data.SE.psi2, corr_data.SE.psi1, s=5, alpha=.4)
pyplot.scatter(corr_data.SE.psi2[sig], corr_data.SE.psi1[sig], s=10, c='r', label='sig')
pyplot.xlabel('Control PSI (psi2)')
pyplot.ylabel('KD PSI (psi1)')
```




    Text(0, 0.5, 'KD PSI (psi1)')




![png](docs/images/output_30_1.png)


The effect of shrinkage is most clearly visualized by again considering the relationship between the deltaPSI estimates and the total read count:

```python
pyplot.figure(figsize=(12,5))
ax=pyplot.subplot(121,)
pyplot.title('Before shrinkage')
pyplot.scatter(data.SE.orig.totalreads, data.SE.orig.dpsi, s=5, alpha=.2,)
pyplot.scatter(data.SE.orig.totalreads[sig], data.SE.orig.dpsi[sig], c='r', s=5, alpha=.2, label='Sig')
pyplot.xscale('log')
pyplot.ylabel('dPSI')
pyplot.xlabel('Average IJC + SJC (log10)')
pyplot.subplot(122,sharey=ax)
pyplot.title('After shrinkage')
pyplot.scatter(corr_data.SE.totalreads, corr_data.SE.dpsi, s=5,alpha=.2)
pyplot.scatter(corr_data.SE.totalreads[sig], corr_data.SE.dpsi[sig], c='r', s=5, alpha=.2,label='Sig')
pyplot.xscale('log')
pyplot.ylabel('dPSI')
pyplot.xlabel('Average IJC + SJC (log10)')
```




    Text(0.5, 0, 'Average IJC + SJC (log10)')




![png](docs/images/output_32_1.png)


We can also directly compare how the dPSI estimate differ before and after performing shrinkage

```python
pyplot.scatter(data.SE.dpsi,corr_data.SE.dpsi, s=5, c='b',alpha=.5, label='Not sig')
pyplot.gca().set_facecolor('lightgrey')
pyplot.scatter(data.SE.dpsi[sig],corr_data.SE.dpsi[sig], s=15,c='r', label='Sig')
pyplot.ylabel('dPSI (after shrinkage)')
pyplot.xlabel('dPSI (before shrinkage)')
pyplot.legend()
pyplot.plot([-1,1],[-1,1], c='black', lw=2, ls='--',)
minval, maxval=numpy.min(data.SE.dpsi), numpy.max(data.SE.dpsi)
xlim=pyplot.xlim(minval-.05,maxval+.05)
ylim=pyplot.ylim(minval-.05,maxval+.05)

```


![png](docs/images/output_34_0.png)


It's might also be helpful to plot how much dPSI decreases after shrinkage as a function of the readcounts:

```python
pyplot.scatter(data.SE.totalreads[sig], numpy.abs(data.SE.dpsi[sig]-corr_data.SE.dpsi[sig]))
pyplot.xlim(0,100)
pyplot.ylabel('Amount of shrinkage')
pyplot.xlabel('Average read count (IJC+SJC)')
```




    Text(0.5, 0, 'Average read count (IJC+SJC)')




![png](docs/images/output_36_1.png)


### Writing the corrected estimates to output



```python
corr_data.write2csv('01_outputs/rMATS', outname='corrected')
```
